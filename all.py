# -*- coding: utf-8 -*-
import xbmc,urllib

all_modules = [ 'https://bitbucket.org/commanche2/p2p-streams-parsers/src/77e58b0822fbd724bab5538b8fae0bd0318a404f/Wiziwig.tv/wiziwig.tar.gz?at=master', 'https://bitbucket.org/commanche2/p2p-streams-parsers/src/77e58b0822fbd724bab5538b8fae0bd0318a404f/Arenavision.in/arenavision.tar.gz?at=master', 'https://bitbucket.org/commanche2/p2p-streams-parsers/src/77e58b0822fbd724bab5538b8fae0bd0318a404f/Livefootball.ws/livefootballws.tar.gz?at=master', 'https://bitbucket.org/commanche2/p2p-streams-parsers/src/77e58b0822fbd724bab5538b8fae0bd0318a404f/Livefootballvideo.com/livefootballvideo.tar.gz?at=master', 'https://bitbucket.org/commanche2/p2p-streams-parsers/src/77e58b0822fbd724bab5538b8fae0bd0318a404f/Livefootballaol.com/livefootballaol.tar.gz?at=master','https://bitbucket.org/commanche2/p2p-streams-parsers/src/77e58b0822fbd724bab5538b8fae0bd0318a404f/Rojadirecta.me/rojadirecta.tar.gz?at=master','https://bitbucket.org/commanche2/p2p-streams-parsers/src/77e58b0822fbd724bab5538b8fae0bd0318a404f/SopCast.ucoz/sopcastucoz.tar.gz?at=master','https://bitbucket.org/commanche2/p2p-streams-parsers/src/77e58b0822fbd724bab5538b8fae0bd0318a404f/Torrent-tv.ru%20all/torrenttvruall.tar.gz?at=master','https://bitbucket.org/commanche2/p2p-streams-parsers/src/77e58b0822fbd724bab5538b8fae0bd0318a404f/Torrent-tv.ru%20sports/torrenttvrusports.tar.gz?at=master','https://bitbucket.org/commanche2/p2p-streams-parsers/src/77e58b0822fbd724bab5538b8fae0bd0318a404f/1Torrent.tv/onetorrenttv.tar.gz?at=master']

for parser in all_modules:
	xbmc.executebuiltin('XBMC.RunPlugin("plugin://plugin.video.p2p-streams/?mode=405&name=p2p&url=' + urllib.quote(parser) + '")')
	xbmc.sleep(1000)

xbmc.executebuiltin("Notification(%s,%s,%i,%s)" % ('P2P-Streams', "All parsers imported",1,''))
